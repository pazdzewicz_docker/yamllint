# yamllint

This Image contains yamllint and a few tools

## Parent image

- `debian:bookworm`

## Dependencies

None

## Entrypoint & CMD

Since this Image is only used during CI we don't need an entrypoint.

## Functions

- `yamllint .` - Executes yamllint

## Build Image

### Automated Build

Through the new build pipeline all of our Docker images are built via Gitlab CI/CD and pushed to the Gitlab Container Registry. You usually don't need to build the image on your own, just push your commits to the git. The Image will be tagged after your branch name.

After the automated build is finished you can pull the image:

```
docker pull registry.gitlab.com/pazdzewicz_docker/yamllint:BRANCH
```

All images also have a master tag, just replace the BRANCH with "master". The "latest" Tag is only an alias for the "master" tag.

### Manual Build

You can also build the Docker image on your local pc:

```
docker build -t "myyamllint:latest" .
```

### Build Options

Build Options are Arguments in the Dockerfile (`--build-arg`):

- None

### Security Check during Build

Before we release Docker Containers into the wild it is required to run the Anchore security checks over the Pipeline (this doesn't apply to manual built images). You can download the current artifacts on the Pipelines page (the download drop-down).

## Environment Variables:

## Ports

None

## Usage

### Local Usage

#### Docker Run

```
docker run -v "./:/data" registry.gitlab.com/pazdzewicz_docker/yamllint:latest yamllint /data
```

#### Docker Compose

***Command***

```
docker-compose run yamllint yamllint /data
```

***docker-compose.yml***

```
version: '3'

services:
  yamllint:
    image: registry.gitlab.com/pazdzewicz_docker/yamllint:latest
    build: .
    volumes:
      - "./:/data"
```

#### Gitlab CI

```
####
# Lint YAML Files
####

yamllint:
  stage: lint
  image:
    name: registry.gitlab.com/pazdzewicz_docker/yamllint:latest
  script:
  - yamllint .
```